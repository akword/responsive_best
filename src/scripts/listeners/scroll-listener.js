var $TBG = $TBG || {};

var scrollListenerDebounceFunction = debounce(function() {
  'use strict';
  
  if(parseInt($(window).scrollTop()) > $('#header').height()) {
    $('#header').addClass('compact');
		//$TBG.videoOverlay.openVideo($TBG.videoOverlay.videoSource);
  } else {
    $('#header').removeClass('compact');
		//$TBG.videoOverlay.closeVideo();
  }
  
}, 100);

window.addEventListener('scroll', scrollListenerDebounceFunction);