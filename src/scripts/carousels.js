var $HB = $HB || {};

$HB.homeCarouselControls = {
  
  init : function() {
    'use strict';
    var _this = this;
    
    // Previous Slide Button
    $(document).on('click', '#home-carousel-nav-left', function(e){
      e.preventDefault();

      if(!$('#home-carousel-nav-left').hasClass('disabled')) {
        $HB.homeCarousel.trigger('prev.owl.carousel');
      }

    });

    // Next Slide Button
    $(document).on('click', '#home-carousel-nav-right', function(e){
      e.preventDefault();

      if(!$('#home-carousel-nav-right').hasClass('disabled')) {
        $HB.homeCarousel.trigger('next.owl.carousel');
      }
    });
    
  }, 
  
  adjustButtons : function(event) {
    'use strict';
    var _this = this;
    
    $('.home-carousel-nav').removeClass('disabled');
    
    var index = event.item.index;
    
    if(!isNaN(index)) {
      
      if(index === 0) {
        $('#home-carousel-nav-left').addClass('disabled');
      }
      
      if(index >= (event.item.count - 1)) {
        $('#home-carousel-nav-right').addClass('disabled');
      }
    }
  }, 
  
  adjustSlideHeight : function() {
    'use strict';
    var _this = this;
    
      var $adjustedHeight = parseInt($(window).height() - ($('#header-desktop').height() + $('#sign-up-bar').outerHeight()));
      var $adjustedHeightMin = 400;

      if($adjustedHeight >= $adjustedHeightMin && !isMobile.any && $HB.screenwidth > $TBG.breakPoints.sm) {
        $('#home-carousel .item').height($adjustedHeight);
      } else {
        $('#home-carousel .item').height('');
      }
  }
  
};

jQuery(function($) {
  'use strict';
  
  $HB.homeCarousel = $('#home-carousel');
  $HB.homeCarousel.owlCarousel({
    items: 1,
    loop: true,
    center: true,
    margin: 0,
    responsiveClass: true,
    URLhashListener: true,
    autoplay: true,
    autoplayHoverPause: true,
    startPosition: 'URLHash',
    video:true,
    responsive:{
      0:{
        items: 1,
        nav: false
      },
      768:{
        items: 1,
        nav: false
      },
      992:{
        items: 1,
        nav: false,
        loop: false
      }
    }, 
    onChanged: callback
  });
  
  function callback(event) {
    $HB.homeCarouselControls.adjustButtons(event);
  }
  
  $HB.homeCarouselControls.init();
  $HB.homeCarouselControls.adjustSlideHeight();
});